import { browser, by, element } from 'protractor';

export class CartPage {
    continueShopping() {
        element(by.css('a[href$=product]')).click();
    }

    navigateToCart() {
        element(by.css('a[href$=cart]')).click();
    }

    buyProduct() {
        element(by.css('div.col-md-3:nth-child(3) > div:nth-child(1) > div:nth-child(2) > p:nth-child(3) > button:nth-child(2)')).click();
    }

    updateQuantity(quantity: number) {
        element(by.css('tr:nth-child(1) input[type=number][name=productQuantity]')).clear();
        element(by.css('tr:nth-child(1) input[type=number][name=productQuantity]')).sendKeys(quantity);
        return element(by.css('tr:nth-child(1) > td:nth-child(4)'));
    }

    getDeleteButton() {
        return element(by.css('button.btn'));
    }

    getTotalCartAmount() {
        let totalAmountInCart;
        return element(by.css('.total-amount')).getText().then(text => {
            return totalAmountInCart += Number(text.substring(1));
        });
    }

    getPrice(price: string) {
        return +(price.substring(1));
    }

    checkout() {
        element(by.css('.btn-checkout')).click();
    }
}
