import { browser, by, element } from 'protractor';
import { CartPage } from './cart.po';
import { Helper } from '../common/helper-class';

describe('Cart page', () => {
    const page = new CartPage();
    const helper = new Helper();

    it('should display no cart items message if there are no items', () => {
        helper.navigateTo();
        browser.sleep(3000);
        page.navigateToCart();
        expect(element(by.css('.empty-cart-message')).getText()).toEqual('No items in cart!');
    });

    // it('should not display products to unauthorized users', () => {
    //     browser.get('/product');
    //     browser.sleep(3000);
    //     expect(browser.getCurrentUrl()).toContain('login');
    //     expect(element(by.css('input[name=email]')).isPresent()).toBeTruthy();
    //     expect(element(by.css('input[name=password]')).isPresent()).toBeTruthy();
    // });

    it('should display cart items when product is added to cart', () => {
        page.navigateToCart();
        page.continueShopping();
        expect(browser.getCurrentUrl()).toContain('product');
        page.buyProduct();
        expect(browser.getCurrentUrl()).toContain('cart');
        expect(element.all(by.css('table tr')).count()).toBeGreaterThanOrEqual(1);
    });

    it('should display total price of cart items', () => {
        let total = 0;
        let totalAmountInCart;
        element.all(by.css('.table > tbody > tr td:nth-child(4)')).each(elements => {
            elements.getText().then(text => {
                total += Number(text.substring(1));
            })
            .then(() => {
                element(by.css('.total-amount')).getText().then(text => {
                    totalAmountInCart = Number(text.substring(1));
                });
            }).then(() => {
                expect(total).toEqual(totalAmountInCart);
            });
        });
    });

    it('should update price of items when quantity is increased', () => {
        let priceOfProductElement = page.updateQuantity(1);
        const priceOfProductBefore = page.getPrice(priceOfProductElement.getText().toString());
        priceOfProductElement = page.updateQuantity(2);
        const priceOfProductAfter = page.getPrice(priceOfProductElement.getText().toString());
        expect(priceOfProductAfter).toEqual(priceOfProductBefore * 2);
    });

    it('delete cart item should delete the product from cart', () => {
        page.getDeleteButton().click();
        expect(element.all(by.css('table tr')).count()).toEqual(0);
    });

    it('should redirect to checkout after clicking checkout button', () => {
        page.continueShopping();
        expect(browser.getCurrentUrl()).toContain('product');
        page.buyProduct();
        page.checkout();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain('checkout');
    });
});
