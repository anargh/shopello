import { browser, element, by } from 'protractor';
import { Helper } from '../common/helper-class';
import { HeaderPage } from './header.po';

describe('Header Component', () => {
    const page = new HeaderPage();
    const helper = new Helper();

    beforeAll(() => {
        browser.get('/');
    });

    it('should not show category menu to unauthorized users', () => {
        browser.get('/product');
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('login');
        expect(page.getMainMenu().isPresent()).toBeFalsy();
    });

    it('should show main menu to users who are logged in', () => {
        helper.navigateTo();
        expect(browser.getCurrentUrl()).toContain('product');
        expect(page.getMainMenu().isDisplayed()).toBeTruthy();
    });

    it('should display links to logout, cart, orders and profile for logged in users', () => {
        expect(page.getTopNavLinks().isDisplayed()).toBeTruthy();
    });

    it('should redirect user to cart page if clicked on cart link', () => {
        element(by.css('.auth-menu nav a[href$=cart]')).click();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('cart');
    });

    it('should redirect user to orders page if clicked on orders page', () => {
        element(by.css('.auth-menu nav a[href$=orders]')).click();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('orders');
    });

    it('should redirect users to profile page if clicked on profile link', () => {
        element(by.css('.auth-menu nav a[href$=profile]')).click();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('profile');
    });

    it('should repersent links selected with an active class', () => {
        page.clickLink();
        browser.sleep(2000);
        expect(element(by.css(`.main-menu a[href$=${page.linkToSelect}]`)).getAttribute('class')).toContain('active');
    });

    it('should redirect users when clicked on any main menu links', () => {
        page.linkToSelect = 'Laptop';
        page.clickLink();
        expect(browser.getCurrentUrl()).toContain(`${page.linkToSelect}`);
        expect(element(by.css('.product-list')).isDisplayed()).toBeTruthy();
    });

    it('should logout users if clicked on logout link', () => {
        element(by.css('.auth-menu nav a[name=logout]')).click();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('login');
    });

});
