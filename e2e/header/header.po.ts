import { element, by } from 'protractor';

export class HeaderPage {
    public linkToSelect = 'Electronics';
    getMainMenu() {
        return element(by.css('nav.main-menu'));
    }

    clickLink() {
        element(by.css(`.main-menu a[href$=${this.linkToSelect}]`)).click();
    }

    getTopNavLinks() {
        return element.all(by.css('.auth-menu > nav a'));
    }
}
