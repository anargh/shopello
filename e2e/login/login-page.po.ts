import { browser, by, element } from 'protractor';
export class LoginPage {
    clickSignupLink() {
        element(by.css('form a[href$=register]')).click();
    }

    clickNavSignupLink() {
        element(by.css('nav a[href$=register]')).click();
    }
}
