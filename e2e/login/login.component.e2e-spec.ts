import { browser, by, element } from 'protractor';
import { Helper } from '../common/helper-class';
import { LoginPage } from './login-page.po';

describe('Login page', () => {
    let emailElement;
    let passwordElement;
    let loginButton;
    let formElement;
    const page = new LoginPage();
    const helper = new Helper();

    beforeEach(() => {
        browser.get('/login');
        emailElement = element(by.css('input[type=email]'));
        passwordElement = element(by.css('input[type=password'));
        loginButton = element(by.css('button[type=submit]'));
        formElement = element(by.css('form[name=loginForm]'));
    });

    it('should show login form', () => {
        expect(element(by.css('form[name=loginForm]')).isPresent()).toBeTruthy();
        expect(element(by.css('.form-heading')).getText()).toBe('LOGIN');
    });

    it('submit button should be disabled with if email is blank', () => {
        emailElement.sendKeys('');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
    });

    it('should not accept blank password', () => {
        passwordElement.sendKeys('');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
    });
    it('should not accept invalid email input in form', () => {
        emailElement.sendKeys('bad...$#!email@..');
        expect(formElement.getAttribute('class')).toContain('ng-invalid');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
    });

    it('should not accept invalid password input in form', () => {
        passwordElement.sendKeys('123');
        expect(formElement.getAttribute('class')).toContain('ng-invalid');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
    });

    it('should give form a valid class if input is valid', () => {
        emailElement.sendKeys(helper.email);
        passwordElement.sendKeys(helper.password);
        expect(element(by.css('form[name=loginForm]')).getAttribute('class')).toContain('ng-valid');
    });

    it('should show message when invalid login details are given', () => {
        emailElement.sendKeys(helper.email);
        passwordElement.sendKeys('wrongpass');
        loginButton.click();
        browser.sleep(3000);
        expect(element(by.css('.alert-danger p')).getText()).toContain('Username or Password is invalid');
    });

    it('should redirect to product page after login', () => {
        emailElement.sendKeys(helper.email);
        passwordElement.sendKeys(helper.password);
        loginButton.click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain('product');
    });

    it('should redirect to register page when clicked on signup', () => {
        page.clickSignupLink();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('register');
    });

    it('should redirect to register page when clicked on sign up nav menu', () => {
        page.clickNavSignupLink();
        browser.sleep(2000);
        expect(browser.getCurrentUrl()).toContain('register');
    });

    // 10
});
