import { browser, by, element } from 'protractor';

describe('Register page', () => {
    let nameElement;
    let passwordElement;
    let emailElement;
    let confirmPassElement;
    let registerButton;
    beforeEach(() => {
        browser.get('/register');
        nameElement = element(by.css('input[name=name]'));
        passwordElement = element(by.css('input[name=password]'));
        emailElement = element(by.css('input[name=email'));
        confirmPassElement = element(by.css('input[name=confirmPassword'));
        registerButton = element(by.css('input[type=submit'));
    });

    it('submit button should be disabled with no input', () => {
        expect(registerButton.isEnabled()).toBe(false);
    });

    it('should show error message when invalid name is given', () => {
        nameElement.sendKeys('123');
        expect(element(by.css('input[name=name] + div.alert-danger')).isDisplayed()).toBeTruthy();
    });

    it('should show error message when invalid password is given', () => {
        passwordElement.sendKeys('123');
        expect(element(by.css('input[name=password] + div.alert-danger')).isDisplayed()).toBeTruthy();
    });

    it('should show error message when passwords do not match', () => {
        passwordElement.sendKeys('123');
        confirmPassElement.sendKeys('231');
        expect(element(by.css('input[name=confirmPassword] + div.alert-danger')).isDisplayed()).toBeTruthy();
    });

    it('should show error message when email is invalid', () => {
        emailElement.sendKeys('invalid.email@123 ');
        expect(element(by.css('input[name=email] + div.alert-danger')).isDisplayed()).toBeTruthy();
    });

    it('should clear the errors when the invalid name becomes valid', () => {
        nameElement.sendKeys('null');
        browser.sleep(3000);
        expect(element(by.css('input[name=name] + div.alert-danger')).isDisplayed()).toBeTruthy();
        nameElement.clear();
        nameElement.sendKeys('Valid Name');
        browser.sleep(3000);
        expect(element(by.css('input[name=name] + div.alert-danger')).isPresent()).toBeFalsy();
    });

    it('should clear the errors when the invalid password becomes valid', () => {
        passwordElement.sendKeys('123');
        browser.sleep(3000);
        expect(element(by.css('input[name=password] + div.alert-danger')).isDisplayed()).toBeTruthy();
        passwordElement.clear();
        passwordElement.sendKeys('123456validpass');
        browser.sleep(3000);
        expect(element(by.css('input[name=password] + div.alert-danger')).isPresent()).toBeFalsy();
    });

    it('should clear the errors when the password mismatch becomes valid', () => {
        passwordElement.sendKeys('123');
        confirmPassElement.sendKeys('null');
        browser.sleep(3000);
        expect(element(by.css('input[name=confirmPassword] + div.alert-danger')).isDisplayed()).toBeTruthy();
        confirmPassElement.clear();
        confirmPassElement.sendKeys('123');
        browser.sleep(3000);
        expect(element(by.css('input[name=confirmPassword] + div.alert-danger')).isPresent()).toBeFalsy();
    });

    it('should clear the errors when the invalid email becomes valid', () => {
        emailElement.sendKeys('invalidemail');
        browser.sleep(3000);
        expect(element(by.css('input[name=email] + div.alert-danger')).isDisplayed()).toBeTruthy();
        emailElement.clear();
        emailElement.sendKeys('valid@email.com');
        browser.sleep(3000);
        expect(element(by.css('input[name=email] + div.alert-danger')).isPresent()).toBeFalsy();
    });

    it('should show register successful message', () => {
        const pass = 'testpass';
        nameElement.sendKeys('tester');
        passwordElement.sendKeys(pass);
        confirmPassElement.sendKeys(pass);
        emailElement.sendKeys('test@tester.com');
        registerButton.click();
        browser.sleep(3000);
        // expect(element(by.css('div.alert')).isDisplayed()).toBe(true);
        // expect(element(by.css('.alert p')).getText()).toEqual('Registration Successful!');
        expect(browser.getCurrentUrl()).toContain('/login');
    });
});
