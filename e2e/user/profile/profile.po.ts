import { browser, by, element } from 'protractor';

export class ProfilePage {
    navigateToHome() {
        browser.get('/login');
        element(by.css('input[type=email]')).sendKeys('tester@userlogin.com');
        element(by.css('input[type=password]')).sendKeys('testpass');
        element(by.css('button[type=submit]')).click();
        browser.sleep(3000);
    }
    navigateToProfile() {
        element(by.css('a[href$=profile]')).click();
    }

    getNameField() {
        return element(by.css('input[name=name]'));
    }

    getEmailField() {
        return element(by.css('input[name=email]'));
    }

    getStateField() {
        return element(by.css('input[name=state]'));
    }

    getPostCodeField() {
        return element(by.css('input[name=postcode]'));
    }

    getCityField() {
        return element(by.css('input[name=city]'));
    }

    inputValidValues() {
        this.getNameField().sendKeys('Valid name');
        this.getEmailField().sendKeys('validemail@domain.com');
        this.getStateField().sendKeys('Tamil Nadu');
        this.getPostCodeField().sendKeys('600600');
        this.getCityField().sendKeys('Chennai');
    }

    clearFields() {
        this.getNameField().clear();
        this.getEmailField().clear();
        this.getStateField().clear();
        this.getPostCodeField().clear();
        this.getCityField().clear();
    }

    inputInvalidValues() {
        this.getNameField().sendKeys('@#');
        this.getEmailField().sendKeys('invalidemail@domain');
        this.getStateField().sendKeys('0');
        this.getPostCodeField().sendKeys('0');
        this.getCityField().sendKeys('0');
    }
}
