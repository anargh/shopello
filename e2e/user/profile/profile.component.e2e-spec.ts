import { ProfilePage } from './profile.po';
import { element, by, browser } from 'protractor';

describe('Profile page', () => {
    const page = new ProfilePage();

    beforeAll(() => {
        page.navigateToHome();
    });
    beforeEach(() => {
        page.navigateToProfile();
    });

    it('should display profile details page after navigating to /profile', () => {
        expect(browser.getCurrentUrl()).toContain('profile');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
        expect(element(by.css('.form-title')).getText()).toEqual('Profile Details');
    });

    it('should navigate to orders page when clicked on My Orders link', () => {
        element(by.css('a[href$=orders]')).click();
        browser.sleep(1000);
        expect(browser.getCurrentUrl()).toContain('/orders');
    });

    it('should disable submit button if form is invalid', () => {
        page.inputInvalidValues();
        expect(element(by.css('button[type=submit]')).isEnabled()).toBeFalsy();
        page.clearFields();
    });

    it('should disable country field', () => {
        expect(element(by.css('input[name=country]')).isEnabled()).toBeFalsy();
    });

    it('should enable update button if form is valid', () => {
        page.inputValidValues();
        browser.sleep(4000);
        expect(element(by.css('form[name=profileForm]')).getAttribute('class')).toContain('ng-valid');
        expect(element(by.css('button[type=submit]')).isEnabled()).toBeTruthy();
        page.clearFields();
    });

    it('should show error message if invalid name is given', () => {
        page.getNameField().sendKeys('');
        expect(element(by.css('input[name=name] > div.alert-danger')));
    });

    it('should show error message if invalid email is given', () => {
        page.getEmailField().sendKeys('');
        expect(element(by.css('input[name=email] > div.alert-danger')));
    });

    it('should show error message if invalid city is given', () => {
        page.getCityField().sendKeys('');
        expect(element(by.css('input[name=city] > div.alert-danger')));
    });

    it('should show error message if invalid state is given', () => {
        page.getStateField().sendKeys('');
        expect(element(by.css('input[name=state] > div.alert-danger')));
    });

    it('should show error message if invalid postcode is given', () => {
        page.getPostCodeField().sendKeys('');
        expect(element(by.css('input[name=postcode] > div.alert-danger')));
    });

});
