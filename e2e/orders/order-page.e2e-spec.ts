import { browser, by, element } from 'protractor';
import { Helper } from '../common/helper-class';
import { OrderPage } from './order-page.po';

describe('Order Page', () => {
    const page = new OrderPage();

    beforeAll(() => {
        page.navigateTo();
    });

    it('should show a message if there are no products purchased', () => {
        page.navigateToOrders();
        expect(element(by.css('.no-orders')).getText()).toEqual('No history of orders found!');
    });

    it('should show the product purchased in the order history', () => {
        page.navigateTo();
        browser.sleep(3000);
        page.buyProduct();
        expect(browser.getCurrentUrl()).toContain('checkout');
        page.navigateToOrders();
        expect(element(by.css('.orders-table a[href$="021"]')).isPresent()).toBeTruthy();
    });

    it('should show the product detail page for that product, when clicked on product name', () => {
        browser.sleep(4000);
        page.navigateToOrders();
        browser.sleep(4000);
        expect(browser.getCurrentUrl()).toContain('orders');
        browser.sleep(4000);
        element(by.css('.orders-table a[href$="021"]')).click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(page.productId);
        browser.sleep(4000);
        expect(element(by.css('div.col-md-6:nth-child(2) > h2:nth-child(1)')).getText()).toEqual(page.getProductName());
    });
});
