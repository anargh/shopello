import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';
export class OrderPage {

    // public productToBuyQuery = 'div.col-md-3:nth-child(2) > div:nth-child(1) > div:nth-child(2) > h4:nth-child(1) > a:nth-child(1)';
    // public productContainer = 'div.col-md-3:nth-child(2) > div:nth-child(1)';
    // tslint:disable-next-line:quotemark
    public productId = "021";
    public productSearchString = 'Test Product 001';

    navigateTo() {
        browser.get('/login');
        element(by.css('input[type=email]')).sendKeys('test.order@order.com');
        element(by.css('input[type=password]')).sendKeys('123456');
        element(by.css('button[type=submit]')).click();
    }

    navigateToOrders() {
        element(by.css('a[href$=orders]')).click();
    }

    buyProduct() {
        this.searchProduct();
        element(by.css('button.buy-product')).click();
    }

    getProductName() {
        return this.productSearchString;
    }

    searchProduct() {
        element(by.css('.search-product')).sendKeys(this.productSearchString);
        element(by.css('.search-product')).sendKeys(protractor.Key.ENTER);
        element(by.css('.product-list a[href$="021"]')).click();
        browser.sleep(3000);
    }
}
