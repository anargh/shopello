import { AppPage } from './app.po';
describe('shopello App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getLogoText()).toEqual('Shopello');
  });
});
