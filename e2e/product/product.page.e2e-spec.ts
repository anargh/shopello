import { Helper } from '../common/helper-class';
import { browser, by, element } from 'protractor';
import { ProductPage } from './product-page.po';
import { PAGINATION } from '../../src/environments/environment';
import { protractor } from 'protractor/built/ptor';

describe('Product page', () => {
    const helper = new Helper();
    const page = new ProductPage();
    const recordsPerPage = PAGINATION.recordsPerPage;

    it('should display list of products', () => {
        helper.navigateTo();
        expect(element(by.css('div.product-list')).isDisplayed()).toBeTruthy();
        expect(element(by.css('input[type=checkbox][value=All]')).isSelected()).toBeTruthy();
    });

    it('should show only limited number of products per page', () => {
        helper.navigateTo();
        expect(element.all(by.css('.product-list .thumbnail')).count()).toBeLessThanOrEqual(recordsPerPage);
    });

    it('should redirect to specific page when clicked on that specific page number', () => {
        helper.navigateTo();
        const pageNumber = 2;
        element(by.css('a[href$="page=2"]')).click();
        expect(browser.getCurrentUrl()).toContain(`page=${pageNumber}`);
    });

    it('should add the product to cart after clicking add to cart button', () => {
        const addToCartButton = element(by
            .css('div.col-md-3:nth-child(2) > div:nth-child(1) > div:nth-child(2) > p:nth-child(3) > button:nth-child(2)'));
        // Add the product to cart
        addToCartButton.click();
        browser.sleep(1000);
        expect(browser.getCurrentUrl()).toContain('/cart');
        // Click on Continue Shopping button
        element(by.css('a.btn:nth-child(1)')).click();
    });

    it('should disable add to cart button for products already added to cart', () => {
        page.searchProduct();
        browser.sleep(3000);
        element(by.css('button.add-cart')).click();
        browser.sleep(3000);
        element(by.css('a[href$=product')).click();
        browser.sleep(3000);
        page.searchProduct();
        browser.sleep(3000);
        expect(element(by.css('button.add-cart')).isEnabled()).toBeFalsy();
    });

    it('should redirect to product detail page when clicked on product link', () => {
        helper.navigateTo();
        const productLink = element(by
            .css('div.col-md-3:nth-child(2) > div:nth-child(1) > div:nth-child(2) > h4:nth-child(1) > a:nth-child(1)'));
        const anchorText = productLink.getText();
        const anchorLink = productLink.getAttribute('href');
        productLink.click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain(anchorLink);
        expect(element(by.css('h2')).getText()).toBe(anchorText);
    });

    it('should buy the product and show checkout page after clicking buy button', () => {
        helper.navigateTo();
        page.buyProduct();
        browser.sleep(1000);
        expect(browser.getCurrentUrl()).toContain('/checkout');
    });

    it('should filter the products based on the selected filter property', () => {
        helper.navigateTo();
        const selectCategory = element(by.css('input[type="checkbox"][value="Electronics"]'));
        selectCategory.click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain('Electronics');
        expect(element(by.css('nav a[href$=Electronics]')).getAttribute('class')).toContain('active');
    });

    it('should uncheck other filters if one of the filter is checked', () => {
        helper.navigateTo();
        const selectCategory = element(by.css('input[type=checkbox][value=Electronics]'));
        selectCategory.click();
        expect(element.all(by.css('input[type=checkbox]:checked')).count()).toEqual(1);
    });

    it('should filter the products based on price when clicked upon', () => {
        const selectPriceFilter = element(by.css('input[type="radio"][name="priceFilter"][data-minvalue="0"][data-maxvalue="500"]'));
        selectPriceFilter.click();
        expect(browser.getCurrentUrl()).toContain('priceLow=0&priceHigh=500');
    });

    it('should search for the product and display the products matching the search string', () => {
        const searchString = 'TEST_PRODUCT_NAME_002';
        page.inputSearchString(searchString);
        element(by.css('.search-product + span > a')).click();
        expect(browser.getCurrentUrl()).toContain(searchString);
    });

    it('should search for product by pressing enter key', () => {
        const searchString = 'TEST_PRODUCT_NAME_002';
        page.inputSearchString(searchString);
        element(by.css('.search-product')).sendKeys(protractor.Key.ENTER);
        expect(browser.getCurrentUrl()).toContain(searchString);
    });

});
