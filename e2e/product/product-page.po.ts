import { browser, element, by, protractor } from 'protractor';
export class ProductPage {
    public productSearchString = 'Test Product 001';
    buyProduct() {
        element(by.css('div.col-md-3:nth-child(3) > div:nth-child(1) > div:nth-child(2) > p:nth-child(3) > button:nth-child(1)')).click();
    }

    inputSearchString(searchString: string) {
        element(by.css('.search-product')).clear();
        element(by.css('.search-product')).sendKeys(searchString);
    }

    searchProduct() {
        element(by.css('.search-product')).clear();
        element(by.css('.search-product')).sendKeys(this.productSearchString);
        element(by.css('.search-product')).sendKeys(protractor.Key.ENTER);
        element(by.css('.product-list a[href$="021"]')).click();
        browser.sleep(3000);
    }
}
