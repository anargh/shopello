import { browser, by, element } from 'protractor';

export class Helper {
    public email = 'tester@userlogin.com';
    public password = 'testpass';
    navigateTo() {
        browser.get('/login');
        element(by.css('input[type=email]')).sendKeys(this.email);
        element(by.css('input[type=password]')).sendKeys(this.password);
        element(by.css('button[type=submit]')).click();
    }
}
