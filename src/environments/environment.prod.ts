export const environment = {
  production: true,
  userRequestUrl : 'http://localhost:3000/users',
  orderRequestUrl: 'http://localhost:3000/orders',
  productRequestUrl: 'http://localhost:3000/products'
};

export const PRODUCT = {
  MAX_PRICE : 9999
};

export const PAGINATION = {
  recordsPerPage : 8,
  initialPage : 1
};
