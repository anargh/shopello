// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userRequestUrl : 'http://localhost:3000/users',
  orderRequestUrl: 'http://localhost:3000/orders',
  productRequestUrl: 'http://localhost:3000/products'
};

export const PRODUCT = {
  MAX_PRICE : 9999
};

export const PAGINATION = {
  recordsPerPage : 8,
  initialPage : 1
};


