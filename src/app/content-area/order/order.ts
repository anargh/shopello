import { CartItem } from '../cart';

export class Order {
    orderID: string;
    userID: number;
    purchaseDate: string;
    orderItems: CartItem[];
    totalAmount: number;
}
