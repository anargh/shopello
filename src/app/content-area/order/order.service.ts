import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Order } from './order';

@Injectable()
export class OrderService {
    constructor(private http: HttpClient) {}

    // Get the order history for that user.
    getOrderDetails(userid: number): Observable<Order[]> {
        return this.http.get<Order[]>(`${environment.orderRequestUrl}?userID=${userid}`);
    }
}
