import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../order/order.service';
import { Order } from '../../order';
import { AuthenticateService } from '../../common';

@Component({
    selector: 'app-order-history',
    templateUrl: './order-history.component.html',
    styles: []
})
export class OrderHistoryComponent implements OnInit {

    public orders: Order[];
    constructor(private orderService: OrderService,
                private authService: AuthenticateService) { }

    ngOnInit() {

        // Fetch the order history details of the user.
        this.orderService.getOrderDetails(Number(this.authService.loggedInUserID))
        .subscribe(result => {
            this.orders = result;
            console.log(this.orders.length);
        });
    }

}
