import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { OrderHistoryComponent } from './order-history.component';
import { AuthenticateService } from '../../common/services/authenticate.service';
import { MockAuthenticateService } from '../../../../testing/services/mock-auth.service';
import { OrderService, Order } from '../index';
import { Observable } from 'rxjs/Observable';
import { CartService } from '../../cart/cart.service';

const order: Order[] = [{
    'orderID': 'TESTID',
    'userID': 1,
    'purchaseDate': '1/27/2018',
    'orderItems': [
        {
        'product': {
            'id': 1,
            'name': 'Indigo Shirt for men',
            'description': 'At vero eos et accusamus et iusto odio dignissimos',
            'price': 1000
        },
        'userID': 1,
        'quantity': 1
        }
    ],
    'totalAmount': 151500
}];

class FakeOrderService {

    getOrderDetails(userid: number = 1): Observable<Order[]> {
      return Observable.of(order);
    }
}

describe('OrderHistoryComponent', () => {
    let component: OrderHistoryComponent;
    let fixture: ComponentFixture<OrderHistoryComponent>;
    let orderService: OrderService;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [ OrderHistoryComponent ],
        providers: [{provide: AuthenticateService, useValue: {}},
                    {provide: OrderService, useClass: FakeOrderService}]
        })
        .overrideComponent(OrderHistoryComponent, {
        set: {
            providers: [
                {provide: OrderService, useClass: FakeOrderService }
            ]
        }
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OrderHistoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        orderService = TestBed.get(OrderService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call order service', () => {
        component.ngOnInit();
        spyOn(orderService, 'getOrderDetails').and.callThrough();
        expect(component.orders).toBe(order);
    });
});
