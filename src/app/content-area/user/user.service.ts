import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../user';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService {
    private registerRequestUrl = environment.userRequestUrl;
    private userUrl = environment.userRequestUrl;

    constructor(private http: HttpClient) { }

    // Register user method
    registerUser(user: User): Observable<any> {
        return this.http.post(this.registerRequestUrl, user, {observe: 'response'});
    }

    // Fetch the user details of the authenticated user.
    getUserDetails(userid: string): Observable<any> {
        return this.http.get(`${this.userUrl}?id=${userid}`);
    }

    updateUserDetails(userid: string, user: User): Observable<any> {
        return this.http.patch(`${this.userUrl}/${userid}`, user);
    }
}
