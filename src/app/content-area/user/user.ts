export class User {
    name?: string;
    password?: string;
    role?: string;
    email?: string;
    city?: string;
    state?: string;
    postcode?: number;
    country?: string;
}
