import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { RegisterComponent } from './register.component';
import { UserService } from '../user.service';
import { MockUserService } from '../../../../testing/services/mock-user.service';
import { LoginComponent } from '../../login';
import { User } from '../user';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let userService: MockUserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule,
                RouterTestingModule.withRoutes(
          [{path: 'login', component: LoginComponent}])],
      declarations: [ RegisterComponent, LoginComponent ],
      providers: [{provide: UserService, useValue: { } }]
    })
    .overrideComponent(RegisterComponent, {
      set: {
        providers: [
          { provide: UserService, useClass: MockUserService }
        ]
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = TestBed.get(UserService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('register user', () => {
    let fakeResponse = null;
    const user: User = {email: 'test@gmail.com', password: '123456'};
    userService.registerUser(user).subscribe((value) => {
      fakeResponse = value;
    });
    expect(fakeResponse).toBe({email: 'test@gmail.com', password: '123456'});
  });
});
