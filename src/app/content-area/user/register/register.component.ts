import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from './../user.service';
import { User } from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = new User();

  confirmPasswordInput: string;
  errorMessage = '';
  successMessage = '';

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  registerUser() {
    // Send HTTP post request for saving the details of user.
    this.user.role = 'regular'; // TODO: Find a better way to do this.
    this.userService.registerUser(this.user).subscribe(response => {
      if (response.status === 201) {
        this.successMessage = 'Registration Successful';
        setTimeout(() => this.router.navigate(['/login']), 700);
      } else {
        this.errorMessage = 'Registration failed! Try again Later!';
      }
    },
    error => { this.errorMessage = `We are experiencing problems with the backend: ${error.message}`; });
  }
}
