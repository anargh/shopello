import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from './../../user.service';
import { AuthenticateService } from '../../../common';
import { User } from './../../user';
import { ValidateEmail } from '../../../common/validators/email-validator-reactive.directive';

@Component({
    selector: 'app-profile-detail',
    templateUrl: './profile-detail.component.html',
    styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {

    user = new User();
    userID: string;
    profileForm: FormGroup;
    formErrors = {
        name: '',
        email: '',
        city: '',
        state: '',
        postcode: '',
        country: ''
    };

    successMessage: string;
    errorMessage: string;

    constructor(private authService: AuthenticateService,
                private userService: UserService) {  }

    ngOnInit() {
        this.userID = this.authService.loggedInUserID;
        this.profileForm = new FormGroup({
            name: new FormControl(
                                { value: this.user.name, disabled: false },
                                [Validators.required,
                                Validators.minLength(5),
                                Validators.maxLength(30)]
                            ),
            email: new FormControl(
                                this.user.email,
                                [Validators.required,
                                ValidateEmail]
                            ),
            city: new FormControl(
                                this.user.city,
                                [Validators.required,
                                Validators.minLength(3)]),
            state: new FormControl(
                                this.user.state,
                                [Validators.required,
                                Validators.minLength(3)]),
            postcode: new FormControl(
                                this.user.postcode,
                                [Validators.required,
                                Validators.minLength(6),
                                Validators.maxLength(6)]),
            country: new FormControl(
                                { value: 'India', disabled: true},
                                [Validators.required])
        });
        this.getProfileDetails();
    }

    getProfileDetails() {
        this.userService.getUserDetails(this.userID).subscribe(result => {
            if (result.length  > 0) {
                this.user = result[0];
                console.log(this.user);
            }
        });
    }

    updateDetails() {
        console.log(this.profileForm, this.profileForm.status, this.profileForm.valid);
        this.userService.updateUserDetails(this.userID, this.user).subscribe(result => {
           if (this.user === result) {
               this.successMessage = 'Profile has been succesfully updated';
           } else {
               this.errorMessage = 'Profile not updated. Please try again later.';
           }
        },
        error => {
            this.errorMessage = `ERROR: ${error.message}`;
        }
        );
    }

}
