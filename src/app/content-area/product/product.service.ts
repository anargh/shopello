import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Product } from './product';
import { FilterProduct } from './filter-models/filter-product';
import { Observable } from 'rxjs/Observable';
import { environment, PAGINATION } from '../../../environments/environment';

@Injectable()
export class ProductService {

    private requestURL = environment.productRequestUrl;
    private recordsPerPage = PAGINATION.recordsPerPage;
    private productRequestUrl: string;
    public productIdToEdit: number;

    constructor(private http: HttpClient) { }

    // Fetch all the products for listing.
    getProducts(pageNumber: number = 1, filterOptions: FilterProduct = {}): Observable<any> {

        // If subcategory is selected, change request URL to reflect it
        if (filterOptions.hasOwnProperty('subcategory')) {
            this.productRequestUrl = `${this.requestURL}?subcategory=${filterOptions.subcategory}&_page=${pageNumber}`;
        } else if (filterOptions.hasOwnProperty('category')) { // If category is selected, change request URL to reflect it
            this.productRequestUrl = filterOptions.category === 'All' ?
                                    `${this.requestURL}?_page=${pageNumber}` :
                                    `${this.requestURL}?category=${filterOptions.category}&_page=${pageNumber}`;
        } else { // Else request all products to fetch
            this.productRequestUrl = `${this.requestURL}?_page=${pageNumber}`;
        }

        // If price filter is given, make changes in request URL
        if (filterOptions.hasOwnProperty('priceLow') || filterOptions.hasOwnProperty('priceHigh')) {
            this.productRequestUrl = `${this.productRequestUrl}&price_gte=${filterOptions.priceLow}&price_lte=${filterOptions.priceHigh}`;
        }
        return this.http
            .get(`${this.productRequestUrl}&_limit=${this.recordsPerPage}`,
            {observe: 'response', responseType: 'json'});
    }

    // Search for the products matching the search string.
    searchProducts(pageNumber: number = 1, searchString: string = ''): Observable<any> {
        this.productRequestUrl = `${this.requestURL}?q=${searchString}&_page=${pageNumber}`;
        return this.http
            .get(`${this.productRequestUrl}&_limit=${this.recordsPerPage}`,
            {observe: 'response', responseType: 'json'});
    }

    // Get detailed specification of the specific product
    getProduct(productId: number) {
        this.productRequestUrl = `${this.requestURL}?id=${productId}`;
        return this.http.get<Product>(this.productRequestUrl);
    }

    addProduct(product: Product) {
        return this.http.post(this.requestURL, product, {observe: 'response'});
    }

    updateProduct(product: Product, productId: number) {
        return this.http.patch(`${this.requestURL}?id=${productId}`, product);
    }

    deleteProduct(productId: number) {
        return this.http.delete(`${this.requestURL}?id=${productId}`);
    }
}
