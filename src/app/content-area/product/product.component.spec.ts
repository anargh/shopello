import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ProductService } from './product.service';
import { ProductComponent } from './product.component';
import { Product } from './index';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { RouterTestingModule } from '@angular/router/testing';
import { MockBackend } from '@angular/http/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthenticateService } from '../common/services';
import { MockAuthenticateService } from '../../../testing/services/mock-auth.service';
import { CartService } from '../cart/cart.service';

class MockProductService {
    products: Product[] = [{
        'id': 1,
        'name': 'Test Product 1',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 100,
        'category': 'Men',
        'subcategory': 'Shirts',
        'supplier': 'Ello Retail',
        'quantity': '2500',
        'image': 'no-image'
    },
    {
        'id': 2,
        'name': 'Test Product 2',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 100,
        'category': 'Men',
        'subcategory': 'Shirts',
        'supplier': 'Ello Retail',
        'quantity': '2500',
        'image': 'no-imagee'
    }];

    getProducts() {
        return Observable.of(this.products);
    }
}
describe('ProductComponent', () => {
    let component: ProductComponent;
    let fixture: ComponentFixture<ProductComponent>;
    let productService: ProductService;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
        declarations: [ ProductComponent ],
        providers: [
            HttpTestingController,
            {provide: AuthenticateService, useValue: {}},
            {provide: CartService, useValue: {}},
            {provide: ProductService, useValue: {}}]
        })
        .overrideComponent(ProductComponent, {
        set: {
            providers: [
            {provide: ProductService, useClass: MockProductService}
            ]
        }
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        productService = fixture.debugElement.injector.get(ProductService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should return list of products', () => {
        let fakeResponse = null;
        const response: Product[] = [{
        'id': 1,
        'name': 'Test Product 1',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 100,
        'category': 'Men',
        'subcategory': 'Shirts',
        'supplier': 'Ello Retail',
        'quantity': '2500',
        'image': 'no-image'
        },
        {
        'id': 2,
        'name': 'Test Product 2',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 100,
        'category': 'Men',
        'subcategory': 'Shirts',
        'supplier': 'Ello Retail',
        'quantity': '2500',
        'image': 'no-imagee'
        }];
        productService.getProducts().subscribe(result => {
        fakeResponse = result;
        });
        expect(fakeResponse).toEqual(response);
    });
    // it('should return list of products', async(() => {
    //   const productList: Product[] = [];
    //   spyOn(MockProductService, 'getProducts').and.returnValue(of(response));
    //   component.getProducts();
    //   fixture.detectChanges();
    //   expect(component).toEqual(response);
    //   expect(true).toBeTruthy();
    // }));
});
