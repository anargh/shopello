import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from './product.service';
import { FilterProduct } from './filter-models/filter-product';
import { PriceFilter } from './filter-models/price-filter';
import { Product } from './product';
import { MENU_ITEMS } from './menu-items';
import { PRICE_FILTER_ITEMS } from './price-filter-items';
import { AuthGuardService, AuthenticateService } from '../common/services';
import { CartService } from '../cart';
import { PRODUCT, PAGINATION } from '../../../environments/environment';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    menu = MENU_ITEMS;
    priceFilterItems = PRICE_FILTER_ITEMS;
    loading: boolean;
    products: Product[];
    selectedCategory = '';
    priceLow: number;
    priceHigh: number;

    // PRODUCT LISTING PROPERTIES
    // PAGINATION PROPERTIES
    recordsPerPage = PAGINATION.recordsPerPage;
    initialPage = PAGINATION.initialPage;
    errorMessage: string;

    currentPage: number;
    pagePointer = 1;
    totalRecords: number;
    totalPages: number;

    searchString: string;

    constructor(private productService: ProductService,
                private router: Router,
                private route: ActivatedRoute,
                private authService: AuthenticateService,
                private cartService: CartService) { }

    ngOnInit() {

        // Fetch all the products initially.
        this.router.navigate(['product'], { queryParams: { category: 'All' } });

        // Observe the query parameters
        this.route.queryParams
            .subscribe(params => {
                if (params) {
                    // If page parameters exist, browse to specific page.
                    if (params.page && params.page > 0) {
                    this.pagePointer = params.page;
                    }
                    // If search parameters exist, show products matching search string,
                    if (params.search) {
                    this.searchProduct(this.pagePointer);
                    } else if (params.subcategory) { // If subcategory params exist, filter the product
                    this.filterByCategory(params);
                    } else if (this.menu.filter(result => result.name === params.category)) {
                        this.filterByCategory(params);
                    }
                } else {
                    this.filterByCategory({ category: 'All'});
                }
            });
        }

    // Browse the products for specific category
    selectCategory(filterOptions: FilterProduct, event: any = null) {
        let categorySelected = false;
        this.menu.forEach(result => {
            if (result.id === event.target.id) {
                result.checked = event.target.checked;
            }
            if (result.checked === true) {
                categorySelected = true;
                if (filterOptions.subcategory && result.sub) {
                    result.sub.forEach(sub_result => {
                        if (sub_result.id === event.target.id) {
                            sub_result.checked = event.target.checked;
                        }
                    });
                }
            }
        });
        if (categorySelected) {
            this.router.navigate(['product'], { queryParams: filterOptions });
        } else {
            this.router.navigate(['product'], { queryParams: { category: 'All' } });
        }
    }

    // Filter the products based on category
    filterByCategory(filterOptions: FilterProduct) {
        this.menu.forEach(result => {
        // Mark the categories by making it checked
            result.checked = false;
            if (result.name === filterOptions.category) {
                result.checked = true;
            }
            if (filterOptions.subcategory && result.sub) {
                result.sub.forEach(sub_result => {
                sub_result.checked = false;
                if (sub_result.name === filterOptions.subcategory) {
                    sub_result.checked = true;
                }
                });
            }
        });
        // Get the products
        this.getProducts(this.pagePointer, filterOptions);
    }
    // Filter the product by Price
    filterByPrice(event) {
        this.pagePointer = 1;
        this.priceFilterItems.forEach(result => {
            result.checked = false;
            if (result.id === event.target.value) {
                result.checked = true;
            }
        });
        // Fetch the minimum price from the data attribute of the element
        this.priceLow = event.target.dataset.minvalue;
        // Fetch the maximum price from the data attribute of the element
        this.priceHigh = event.target.dataset.maxvalue;
        // Navigate to product
        this.router.navigate(['product'],
                    { queryParams: { 'priceLow': this.priceLow, 'priceHigh': this.priceHigh },
                    queryParamsHandling: 'merge' });
        event.target.checked = true;
    }

    // Fetch the total pages for pageination
    getPages(totalPages: number) {
        const pages = [];
        for (let index = 1; index <= totalPages; index++) {
        pages.push(index);
        }
        return pages;
    }

    // Fetch the products for the specific category
    getProducts(page: number = 1, selectedCategory: FilterProduct = {}) {
        this.loading = true;
        this.currentPage = page;
        this.productService.getProducts(page, selectedCategory)
        .subscribe(data => {
            // Get json from respon body
            this.products = data.body;
            this.loading = false;
            // Update the total records of product
            this.totalRecords = +data.headers.get('X-Total-Count');
        },
            error => {
            this.errorMessage = `ERROR: Unable to fetch products. Returned error message: ${error.message}`;
            },
            () => {
                this.loading = false;
                // Update the total pages of fetched products
                this.totalPages = Math.ceil(this.totalRecords / this.recordsPerPage);
            }
        );
    }

    // Search the product
    searchProduct(pageNumber: number = 1) {
        this.productService.searchProducts(pageNumber, this.searchString)
        .subscribe(data => {
            // Get json from respon body
            this.products = data.body;
            this.loading = false;
            // Update the total records of product
            this.totalRecords = +data.headers.get('X-Total-Count');
        },
            error => {
            console.log(error);
            },
            () => {
                // Update the total pages of fetched products
                this.totalPages = Math.ceil(this.totalRecords / this.recordsPerPage);
            }
        );
    }

    // Add the product to cart
    addToCart(product: Product) {
        const userID = +this.authService.loggedInUserID; // Prefix with + operator for converting to number
        // Add the product to cart service.
        this.cartService.addToCart(product, userID);
        this.router.navigate(['cart']);
    }

    // Buy Product
    buyProduct(product: Product) {
        this.cartService.buyProduct(product, Number(this.authService.loggedInUserID)).subscribe(response => {
            if (response.status === 201) {
            // Navigate to "Checkout component"
            this.router.navigate(['/checkout']);
            }
        });
    }

    // Check if item in cart
    isItemInCart(product: Product): boolean {
        return this.cartService.isItemInCart(product);
    }
}
