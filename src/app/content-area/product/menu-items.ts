export class Menu {
    id: string;
    name: string;
    checked: boolean;
    sub?: Menu[];
}
export const MENU_ITEMS: Menu[] = [
    {
        id: '0',
        name: 'All',
        checked: true
    },
    {
        id: '1',
        name: 'Electronics',
        checked: false,
        sub:
            [
            {
                id: '1a',
                name: 'Laptop',
                checked: false
            },
            {
                id: '1b',
                name: 'Mobiles',
                checked: false
            },
            {
                id: '1c',
                name: 'TV',
                checked: false
            }
            ]
    },
    {
        id: '2',
        name: 'Home Appliance',
        checked: false,
        sub:
            [
            {
                id: '2a',
                name: 'Refrigerators',
                checked: false
            },
            {
                id: '2b',
                name: 'Washing Machines',
                checked: false
            },
            {
                id: '2c',
                name: 'Heater',
                checked: false
            }
        ]
    },
    {
        id: '3',
        name: 'Men',
        checked: false,
        sub:
            [
            {
                id: '3a',
                name: 'Shirts',
                checked: false
            },
            {
                id: '3b',
                name: 'Pants',
                checked: false
            },
            {
                id: '3c',
                name: 'Watches',
                checked: false
            }
        ]
    },
    {
        id: '4',
        name: 'Women',
        checked: false,
        sub:
            [
            {
                id: '4a',
                name: 'Shirts',
                checked: false
            },
            {
                id: '4b',
                name: 'Pants',
                checked: false
            },
            {
                id: '4c',
                name: 'Watches',
                checked: false
            }
        ]
    },
    {
        id: '5',
        name: 'Kids And Toys',
        checked: false
    },
    {
        id: '6',
        name: 'Furniture',
        checked: false
    }
];
