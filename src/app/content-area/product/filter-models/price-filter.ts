

export class PriceFilter {
    id: string;
    name: string;
    checked: boolean;
    value: {
        min: number;
        max: number;
    };
}
