export class FilterProduct {
    category?: string;
    subcategory?: string;
    priceLow?: number;
    priceHigh?: number;
}
