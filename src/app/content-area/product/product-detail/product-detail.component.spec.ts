import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDetailComponent } from './product-detail.component';
import { CartService } from '../../cart/';
import { ProductService } from '../product.service';
import { Product } from '../product';
import { AuthenticateService } from '../../common/services';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';

class MockProductService {
    product: Product = {
        'id': 1,
        'name': 'Test Product 2',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 100,
        'category': 'Men',
        'subcategory': 'Shirts',
        'supplier': 'Ello Retail',
        'quantity': '2500',
        'image': 'no-imagee'
    };
    getProduct(id: number) {
        return Observable.of(this.product);
    }
}

describe('ProductDetailComponent', () => {
    let component: ProductDetailComponent;
    let fixture: ComponentFixture<ProductDetailComponent>;
    let productService: ProductService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ProductDetailComponent ],
            providers: [
                ActivatedRoute, Router
            ]
        })
        .overrideComponent(ProductDetailComponent, {
            set: {
                providers: [
                    {provide: CartService, useValue: {}},
                    {provide: AuthenticateService, useValue: {}},
                    {provide: ProductService, useClass: MockProductService}
                ]
            }
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProductDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        productService = fixture.debugElement.injector.get(ProductService);
    });

    it('should get product details of product id 1', () => {
        let fakeResponse = null;
        const response = {
            'id': 1,
            'name': 'Test Product 2',
            'description': 'At vero eos et accusamus et iusto odio dignissimos',
            'price': 100,
            'category': 'Men',
            'subcategory': 'Shirts',
            'supplier': 'Ello Retail',
            'quantity': '2500',
            'image': 'no-imagee'
        };
        productService.getProduct(1).subscribe( _response => {
            fakeResponse = _response;
            console.log(fakeResponse);
        });
        expect(fakeResponse).toEqual(response);
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
