import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { CartService } from '../../cart/';
import { ProductService } from '../product.service';
import { Product } from '../product';
import { AuthenticateService } from '../../common/services';

@Component({
    selector: 'app-product-detail',
    styleUrls: ['product-detail.component.css'],
    templateUrl: 'product-detail.component.html',
    providers: []
})

export class ProductDetailComponent implements OnInit, OnDestroy {

    subscription: any;
    productID: number;
    product: Product;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private productService: ProductService,
                private cartService: CartService,
                private authService: AuthenticateService) {}

    ngOnInit() {
        // Get parent ActivatedRoute of this route.
        console.log('oiii');
        this.subscription = this.route.params.subscribe(params => {
            this.productID = params['id'];

            // Get product details for that product
            this.getProductDetail(this.productID);
        },
        error => console.log(error));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    // Get product detail method
    getProductDetail(productID: number) {
        this.productService.getProduct(productID).subscribe(response => {
            this.product = response;
        });
    }

    // Add the product to cart
    addToCart(product: Product) {
        const userID = +this.authService.loggedInUserID;
        this.cartService.addToCart(product, userID);
        this.router.navigate(['cart']);
    }

    buyProduct(product: Product) {
        this.cartService.buyProduct(product, Number(this.authService.loggedInUserID)).subscribe(response => {
          if (response.status === 201) {
            // Navigate to "Checkout component"
            this.router.navigate(['/checkout']);
          }
        });
    }

    isItemInCart(product: Product): boolean {
        return this.cartService.isItemInCart(product);
    }
}

