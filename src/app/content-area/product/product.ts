export class Product {
    id: number;
    name: string;
    price: number;
    description: string;
    category?: string;
    subcategory?: string;
    supplier?: string;
    quantity?: string;
    image?: string;
}
