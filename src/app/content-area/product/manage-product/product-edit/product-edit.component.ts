import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Product } from '../../product';
import { MENU_ITEMS } from '../../menu-items';

@Component({
    selector: 'app-product-edit',
    templateUrl: './product-edit.component.html',
    styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

    menu = MENU_ITEMS.filter(result => result.name !== 'All');
    message = {
        type: '',
        text: ''
    };

    product = new Product();
    constructor(private productService: ProductService) { }

    ngOnInit() {

        // TODO: ERROR HANDLING AND CHANGE TO ROUTER ID METHOD
        this.productService.getProduct(this.productService.productIdToEdit).subscribe(result => {
        this.product = result[0];
        });
    }

    updateProductDetails() {
        this.productService.updateProduct(this.product, this.productService.productIdToEdit);
    }

}
