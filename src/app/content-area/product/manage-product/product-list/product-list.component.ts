import { Component, OnInit } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from '../../product.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

    products: Product[];
    constructor(private productService: ProductService,
                private router: Router) { }

    ngOnInit() {
        this.getProducts(1);
    }

    getProducts(page: number) {
        this.productService.getProducts(page).subscribe(result => {
            this.products = result.body;
        });
    }

    editProduct(productID: number) {
        this.productService.productIdToEdit = productID;
        this.router.navigate(['/product/manage/edit']);
    }
}
