import { Component, OnInit } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from '../../product.service';
import { MENU_ITEMS } from '../../menu-items';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

    menu = MENU_ITEMS.filter(result => result.name !== 'All');
    product = new Product();
    message = {
        type: '',
        text: ''
    };
    constructor(private productService: ProductService) { }

    ngOnInit() {
    }

    addProduct() {
        this.productService.addProduct(this.product)
            .subscribe(result => {
                if (result.status === 201) {
                        this.message.type = 'success';
                        this.message.text = 'Product added successfully!';
                        setTimeout(() => {
                            this.message.text = '';
                        }, 5000);
                } else {
                        this.message.type = 'error';
                        this.message.text = 'ERROR: There was a problem adding the product,' + result.statusText;
                }
            });
    }
}
