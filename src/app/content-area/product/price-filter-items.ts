import { PRODUCT } from '../../../environments/environment';
import { PriceFilter } from './filter-models/price-filter';
export const PRICE_FILTER_ITEMS: PriceFilter[] = [
    {
        id: '0',
        name: 'Under 500',
        checked: false,
        value: {
            min: 0,
            max: 500
        }
    },
    {
        id: '1',
        name: '500 to 1000',
        checked: false,
        value: {
            min: 500,
            max: 1000
        }
    },
    {
        id: '2',
        name: '1000 to 1500',
        checked: false,
        value: {
            min: 1000,
            max: 1500
        }
    },
    {
        id: '3',
        name: '1500 to 2000',
        checked: false,
        value: {
            min: 1500,
            max: 2000
        }
    },
    {
        id: '4',
        name: 'Above 2000',
        checked: false,
        value: {
            min: 2000,
            max: PRODUCT.MAX_PRICE
        }
    },
];
