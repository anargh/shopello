import { NG_VALIDATORS, AbstractControl, Validator, FormControl } from '@angular/forms';
import { Directive, Attribute } from '@angular/core';

@Directive({
    selector: '[validatePassword][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: PasswordValidatorDirective,
            multi: true
        }
    ]
})

export class PasswordValidatorDirective implements Validator {
    constructor(@Attribute('validatePassword') public validatePassword: string) {}

    validate(input: AbstractControl): { [key: string]: any } {
        // Attribute value
        const password = input.value;

        // Confirmed Password Value
        const confirmPassword = input.root.get(this.validatePassword).value;

        // Compare values
        if (password !== confirmPassword) {
            return {
                validateEqual: true
            };
        }
        return null;
    }
}
