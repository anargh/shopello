// import { FormArray, FormControl, FormGroup, ValidationErrors } from '@angular/forms';

// export class EmailValidator {
//     static validEmail(input: FormControl): ValidationErrors {
//         const email = input.value;
//         let isValidEmail = true;
//         const message = {
//             'vaildEmail': {
//                 'message': 'Should be valid email.'
//             }
//         };
//         if (emailRegEx.test(email)) {
//             isValidEmail = true;
//         } else {
//             isValidEmail = false;
//         }
//         return isValidEmail ? null : message;
//     }
// }

import { AbstractControl } from '@angular/forms';
const emailRegEx =  /^([a-zA-Z0-9](\.|_){0,1})+[A-Za-z0-9]\@([A-Za-z0-9])+\.[a-z]{2,4}$/;

export function ValidateEmail(input: AbstractControl) {
  if (!emailRegEx.test(input.value)) {
    return { validEmail: true };
  }
  return null;
}
