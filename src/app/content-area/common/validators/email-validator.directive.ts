import { NG_VALIDATORS, AbstractControl, ValidatorFn, Validator, FormControl } from '@angular/forms';
import { Directive } from '@angular/core';

function emailValidator(): ValidatorFn {
    return (input: AbstractControl) => {

        const emailRegEx =  /^([a-zA-Z0-9](\.|_){0,1})+[A-Za-z0-9]\@([A-Za-z0-9])+\.[a-z]{2,4}$/;
        if (emailRegEx.test(input.value)) {
            return null;
        } else {
            return {
                emailValidate: true
            };
        }
    };
}

@Directive({
    selector: '[emailValidate][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: EmailValidatorDirective,
            multi: true
        }
    ]
})

export class EmailValidatorDirective implements Validator {
    validator: ValidatorFn;
    constructor() {
        this.validator = emailValidator();
    }

    validate(input: FormControl) {
        return this.validator(input);
    }
}

