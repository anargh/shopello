import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../../services/authenticate.service';
import { Product, MENU_ITEMS } from '../../../product';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

    private authenticatedUser: string;
    private menu = MENU_ITEMS;          // Main menu items

    constructor(private auth: AuthenticateService) { }

    ngOnInit() {
    }

    isAuthenticated(): boolean {
        if (this.auth.isAuthenticated()) {
        // If authenticated, get the username and store it.
        this.authenticatedUser = this.auth.getAuthenticatedUser();
        return true;
        }
        return false;
    }
    isAdmin(): boolean {
        if (this.auth.isAdmin()) {
        return true;
        }
        return false;
    }
}
