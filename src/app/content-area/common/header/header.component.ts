import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { CartService } from '../../cart';
import { MenuComponent } from './menu/menu.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

    constructor(private auth: AuthenticateService,
                private cartService: CartService) { }

    private authenticatedUser: string;
    private totalCartItems = 0;         // Initialize the total cart items variable

    ngOnInit() {
        // Fetch the the total number of items in cart.
        this.cartService.getItems().subscribe(result => {
        this.totalCartItems = result.length;
        });
    }

    // Check whether user is authenticated or not.
    isAuthenticated(): boolean {
        if (this.auth.isAuthenticated()) {
        // If authenticated, get the username and store it.
        this.authenticatedUser = this.auth.getAuthenticatedUser();
        return true;
        }
        return false;
    }

    // Logout method
    logout() {
        this.auth.logout();
    }
}
