import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthenticateService } from '../services/authenticate.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CartService } from '../../cart/cart.service';
import { HeaderComponent } from './header.component';
import {By} from '@angular/platform-browser';

const cartServiceStub = {

};
describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
        declarations: [HeaderComponent],
        providers: [AuthenticateService, { provide: CartService, useValue: cartServiceStub}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should check if user needs login', () => {
        localStorage.clear();
        expect(component.isAuthenticated()).toBeFalsy();
        const navmenu = fixture.debugElement.query(By.css('nav'));
        expect(navmenu).toBeFalsy();
    });

    it('should display login and signup buttons if user is not logged in', () => {
        localStorage.setItem('1', 'test');
        expect(component.isAuthenticated()).toBeTruthy();
        const navmenu = fixture.debugElement.query(By.css('nav'));
        expect(navmenu).toBeFalsy();
    });
});

//  it('should display main menu if user logged in', () => {
//   debugElement = fixture.debugElement.query(By.css('.main-menu'));
//   el = debugElement.nativeElement;
//   authServiceStub.isAuthenticated = true;
//   expect(el.childElementCount).toBeTruthy();
// });