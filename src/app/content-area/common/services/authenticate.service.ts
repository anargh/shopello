import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../../user';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AuthenticateService {

    // The URL to send request to.
    private requestURL = `${environment.userRequestUrl}?email=`;
    loggedInUserID: string;
    constructor(private router: Router,
                private http: HttpClient) { }

    // Authenticate the user
    async checkLogin(email: string, password: string): Promise<any> {
        sessionStorage.clear();
        const isValid = await this.http.get(this.requestURL + email).toPromise()
        .then(response => {
            if (response[0].email === email && response[0].password === password) {
                this.loggedInUserID = response[0].id;
                sessionStorage.setItem(response[0].id, response[0].name);
                if (response[0].role === 'admin') {
                    sessionStorage.setItem(`role${response[0].id}`, 'admin');
                } else {
                    sessionStorage.setItem(`role${response[0].id}`, response[0].role);
                }
                return { 'status': 'true' };
            }
            return { 'status': 'false' };
        })
        .catch(error => {
            return { 'status': 'error', 'message': error.message };
        });
        return isValid;
    }

    // User logout method
    logout() {
        sessionStorage.clear();
        this.router.navigate(['/login']);
    }

    // Check if user is authenticated method
    isAuthenticated(): boolean {
        if (sessionStorage.getItem(this.loggedInUserID)) {
            return true;
        }
        return false;
    }

    // Check if user has admin role!
    isAdmin(): boolean {
        if (sessionStorage.getItem(`role${this.loggedInUserID}`) === 'admin') {
            return true;
        }
        return false;
    }

    // Fetch the authenticated user
    getAuthenticatedUser(): string {
        if (this.isAuthenticated()) {
            return sessionStorage.getItem(this.loggedInUserID);
        }
        return null;
    }
}
