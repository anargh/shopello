import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticateService } from './authenticate.service';

@Injectable()
export class AdminGuardService implements CanActivate {

    constructor(
        private authService: AuthenticateService,
        private router: Router) { }

    canActivate(): boolean {
        if (this.authService.isAuthenticated()) {
        if (this.authService.isAdmin()) {
            return true;
        }
        this.router.navigate(['/product']);
        }
        this.router.navigate(['/']);
        return false;
    }

}
