import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticateService } from './authenticate.service';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
        private auth: AuthenticateService,
        private router: Router
    ) { }

    canActivate(): boolean {
        if (this.auth.isAuthenticated()) {
            if (!this.auth.isAdmin()) {
                return true;
            }
            this.router.navigate(['/product/manage']);
            return true;
        }
        this.router.navigate(['login']);
        sessionStorage.clear();
        return false;
    }
}
