import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateService } from '../common';
import { User } from '../user';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnChanges {

    user = new User();

    errorMessages = [];
    successMessages = [];
    isAuthenticating = false;
    isLoggedIn = false;

    userID: number;
    ngOnInit() {
    }

    ngOnChanges() {
        // If user is authenticated redirect him to product listing page.
        if (this.authenticateService.isAuthenticated()) {
            this.router.navigate(['/product']);
        }
    }

  constructor(private authenticateService: AuthenticateService,
              private router: Router) {}

  // Authenticate user method.

    async login() {
        let message;
        const isValid = await this.authenticateService.checkLogin(this.user.email, this.user.password);
        if (isValid.status === 'true') {
            this.isLoggedIn = true;
            message = 'Login Successful. Redirecting..';
            this.clearMessages();
            this.successMessages.push(message);
            setTimeout(() => {
                if (this.authenticateService.isAdmin()) {
                    // navigate to admin dashboard
                    this.router.navigate(['/product/manage']);
                } else {
                    this.router.navigate(['/product']);
                }
            }, 1000);
        } else if (isValid.status === 'false') {
            this.isLoggedIn = false;
            message = 'Username or Password is invalid';
            this.errorMessages.push(message);
        } else if (isValid.status === 'error') {
            this.isLoggedIn = false;
            message = isValid.message;
            this.errorMessages.push(message);
        }
    }

  // Clear messages method.
    clearMessages() {
        this.errorMessages = [];
        this.successMessages = [];
    }
}
