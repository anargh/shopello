import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticateService } from './../common/services';
import { Router, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { DebugElement } from '@angular/core/src/debug/debug_node';
import {By} from '@angular/platform-browser';
import { MockAuthenticateService } from '../../../testing/services/mock-auth.service';
import { RouterStub } from '../../../testing/services/router-stub';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let emailField: DebugElement;
    let passwordField: DebugElement;
    let loginButton: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpClientModule],
            declarations: [ LoginComponent ],
            providers: [{ provide: Router, useClass: RouterStub }, {provide: AuthenticateService, useClass: MockAuthenticateService}]
            })
            .compileComponents();
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loginButton = fixture.debugElement.query(By.css('button[type=submit]'));
        emailField = fixture.debugElement.query(By.css('input[type=email]'));
        passwordField = fixture.debugElement.query(By.css('input[type=password]'));
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should disable login button if values are empty', () => {
        emailField.nativeElement.value = '';
        passwordField.nativeElement.value = '';
        expect(loginButton.nativeElement.disabled).toBeTruthy();
    });

    it('should authenticate user', () => {
        emailField.nativeElement.value = 'test@gmail.com';
        passwordField.nativeElement.value = '1234567';
        loginButton.nativeElement.click();
        fixture.debugElement.injector.get(AuthenticateService);
        expect(component.login).toBeTruthy();
    });
});
