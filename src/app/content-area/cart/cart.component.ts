import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../common/services';
import { Product } from '../product';
import { CartService } from './cart.service';
import { CartItem } from './cart-item';
import { Router } from '@angular/router';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

    public cartItems: CartItem[];
    constructor(private cartService: CartService,
                private authService: AuthenticateService,
                private router: Router) { }

    public totalCartAmount: number;

    ngOnInit() {
        // Fetch all the products in the cart
        this.cartService.getItems().subscribe(result => {
            this.cartItems = result;

            // Get the total cart amount
            this.totalCartAmount = this.cartService.getTotalCartAmount(Number(this.authService.loggedInUserID));
        });
    }

    // Update the quantity of products
    updateQuantity(item: Product, quantityValue: string) {
            quantityValue = (quantityValue === '') ?  undefined : quantityValue;
            // If value is undefined or 0, remove from cart.
            if (Number(quantityValue) === 0 ) {
            this.removeItemFromCart(item);
            } else {
            this.cartService.updateQuantity(item, Number(quantityValue));
            }
    }

    // Remove item from cart.
    removeItemFromCart(item: Product) {
        this.cartService.removeItemFromCart(item);
    }

    // Confirm order function
    confirmOrder() {
        this.cartService.confirmOrder(this.cartService.cartItems, Number(this.authService.loggedInUserID)).subscribe(response => {
        if (response.status === 201) {
            // Navigate to "Checkout component"
            this.router.navigate(['/checkout']);
        }
        });
    }


}
