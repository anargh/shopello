import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CartService } from './cart.service';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticateService } from '../common/services';
import { CartComponent } from './cart.component';
import { Observable } from 'rxjs/Observable';
import { CartItem } from './cart-item';
import { Product } from '../product/index';

const authServiceStub = {
    isLoggedIn : false,
    isAuthenticated: () => this.isLoggedIn,
};

const totalCartAmount = 1000;
const mockCartItems: CartItem[] = [{
    product: {
        'id': 1,
        'name': 'Indigo Shirt for men',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 1000
    },
    userID: 1,
    quantity: 1
}];

class MockCartService {
    private cartItems: CartItem[] = mockCartItems;
    getItems(): Observable<CartItem[]> {
        console.log('TESTING GETITEMS');
        return Observable.of(this.cartItems);
    }

    getTotalCartAmount() {
        console.log('TESTING CART AMOUNT');
        return totalCartAmount;
    }

    removeItemFromCart() {
        console.log('called');
        this.cartItems = [];
        return Observable.of(this.cartItems);
    }
}
describe('CartComponent', () => {
    let component: CartComponent;
    let fixture: ComponentFixture<CartComponent>;
    let cartService: CartService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [ CartComponent ],
        providers: [{provide: AuthenticateService, useValue: authServiceStub},
                    {provide: CartService, useClass: MockCartService}]
        })
        // .overrideComponent(CartComponent, {
        //   set: {
        //     providers: [{provide: CartService, useClass: MockCartService}]
        //   }
        // })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    cartService = TestBed.get(CartService);
  });

  it('should create', () => {
        expect(component).toBeTruthy();
  });

  it('should set cartitems and totalcartamount', () => {
        component.ngOnInit();
        spyOn(cartService, 'getItems').and.callThrough();
        spyOn(cartService, 'getTotalCartAmount').and.callThrough();
        expect(component.totalCartAmount).toEqual(totalCartAmount);
        expect(component.cartItems).toEqual(mockCartItems);
  });

  // Error! CHECK BACK REQUIRED
  it('should remove item from cart', () => {
        const items = component.cartItems;
        const product: Product = {
        'id': 1,
        'name': 'Indigo Shirt for men',
        'description': 'At vero eos et accusamus et iusto odio dignissimos',
        'price': 1000
    };
    spyOn(cartService, 'removeItemFromCart').and.callThrough();
    expect(cartService.removeItemFromCart).toHaveBeenCalled();
    expect(component.cartItems.length).toBeNull();
  });
});
