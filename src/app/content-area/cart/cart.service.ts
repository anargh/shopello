import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Product } from '../product/product';
import { CartItem } from './cart-item';
import { Order } from '../order';

@Injectable()
export class CartService {
    public cartItemsSubject: BehaviorSubject<CartItem[]> = new BehaviorSubject([]);
    public cartItems: CartItem[] = [];
    private requestURL = environment.orderRequestUrl;

    constructor(private http: HttpClient) {}

    // Confirm order method
    confirmOrder(cartItems: CartItem[], userid: number): Observable<any> {

        // Generate unique ORDER Id.
        const uniqueOrderID = +new Date() + 'USER' + userid;
        const order: Order = {
            orderID: uniqueOrderID,
            userID: userid,
            purchaseDate: new Date().toLocaleDateString(),
            orderItems: cartItems,
            totalAmount: this.getTotalCartAmount(userid)
        };

        // Remove all items from cart for that user.
        this.cartItems = this.cartItems.filter(result => result.userID !== userid);
        this.cartItemsSubject.next(this.cartItems);

        // Update the order history in backend
        return this.http.post(this.requestURL, order, {observe: 'response'});
    }

    // Buy Product
    buyProduct(product: Product, userid: number): Observable<any> {
        const uniqueOrderID = +new Date() + 'USER' + userid;
        const order: Order = {
            orderID: uniqueOrderID,
            userID: userid,
            purchaseDate: new Date().toLocaleDateString(),
            orderItems: [{
                product: product,
                userID: userid,
                quantity: 1
            }],
            totalAmount: product.price
        };
        return this.http.post(this.requestURL, order, {observe: 'response'});
    }

    // Get total cart amount method
    getTotalCartAmount(userid: number) {
        let totalAmount = 0;
        // Find total amount by finding amount for each product.
        this.cartItems.filter(result => result.userID === userid).forEach(result => {
            totalAmount += result.quantity * result.product.price;
          });
        return totalAmount;
    }

    // Add item to cart method
    addToCart(item: Product, userid: number) {
        const cartItem: CartItem = {
            product: {
                id: item.id,
                name: item.name,
                description: item.description,
                price: item.price
            },
            userID: userid,
            quantity: 1
        };

        // If item already exists in cart, update quantity
        if (this.isItemInCart(item)) {
            this.cartItems = this.cartItems
                .filter(result => {
                    if (result.product.id === item.id) {
                        ++result.quantity;
                    }
                    return result;
                }
            );
        } else { // Else add the item to cart
            this.cartItems.push(cartItem);
        }
        // Add the cart items to Subject
        this.cartItemsSubject.next(this.cartItems);
    }

    // Update quantity of items in cart.
    updateQuantity(item: Product, quantityValue: number) {

        // Update the quanity for the product with that specific ID.
        this.cartItems = this.cartItems
                            .filter(result => {
                                if (result.product.id === item.id) {
                                    result.quantity = quantityValue;
                                }
                                return result;
                            }
                        );
        // Update the subject with that specific ID
        this.cartItemsSubject.next(this.cartItems);
    }

    // Remove item from cart method
    removeItemFromCart(item: Product) {
        // Filter the cart items by removing product with that ID
        this.cartItems = this.cartItems.filter(result => result.product.id !== item.id);
        this.cartItemsSubject.next(this.cartItems);
    }

    // Fetch all items in cart
    getItems(): Observable<CartItem[]> {
        return this.cartItemsSubject.asObservable();
    }

    // Method to check if item is in cart.
    isItemInCart(item: Product): boolean {
        let found = false;
        if (this.cartItems.filter(result => result.product.id === item.id).length > 0) {
            found = true;
        }
        return found;
    }
}
