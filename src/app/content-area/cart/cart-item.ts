import { Product } from '../product/product';

export class CartItem {
    product: Product;
    userID: number;
    quantity: number;
}
