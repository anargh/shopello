import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {  HeaderComponent,
          FooterComponent,
          AuthenticateService,
          EmailValidatorDirective,
          PasswordValidatorDirective } from './content-area/common/';
import { LoginComponent } from './content-area/login/';
import { HomeComponent } from './content-area/home/home.component';
import {  ProductComponent,
          ProductDetailComponent,
          ProductService,
          ManageProductComponent,
          ProductAddComponent,
          ProductListComponent,
          ProductEditComponent } from './content-area/product';
import { CartComponent, CartService } from './content-area/cart';
import {  RegisterComponent,
          UserService,
          ProfileComponent,
          ProfileDetailComponent } from './content-area/user';
import {  OrderComponent,
          OrderService,
          CheckoutComponent,
          OrderHistoryComponent } from './content-area/order';
import { MenuComponent } from './content-area/common/header/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProductComponent,
    ProductDetailComponent,
    EmailValidatorDirective,
    PasswordValidatorDirective,
    OrderComponent,
    CartComponent,
    ProfileComponent,
    OrderHistoryComponent,
    ProfileDetailComponent,
    CheckoutComponent,
    MenuComponent,
    ManageProductComponent,
    ProductAddComponent,
    ProductListComponent,
    ProductEditComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule.withServerTransition({ appId: 'shopello'}),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [AuthenticateService, ProductService, CartService, UserService, OrderService],
  bootstrap: [AppComponent]
})

export class AppModule { }
