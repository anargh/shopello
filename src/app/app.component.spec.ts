import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from './content-area/common/header/header.component';
import { AuthenticateService } from './content-area/common/services';
import { CartService } from './content-area/cart/cart.service';


const authServiceStub = {
  isLoggedIn : false,
  isAuthenticated: () => this.isLoggedIn,
};
const cartServiceStub = {

};
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
      providers: [{provide: AuthenticateService, useValue: authServiceStub },
                  {provide: CartService, useValue: cartServiceStub}]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
