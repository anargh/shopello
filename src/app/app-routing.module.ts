import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './content-area/login';
import { HomeComponent } from './content-area/home/home.component';
import { ProductComponent,
         ProductDetailComponent,
         ManageProductComponent,
         ProductAddComponent,
         ProductListComponent,
         ProductEditComponent } from './content-area/product';
import { AuthGuardService as AuthGuard,
         AdminGuardService as AdminGuard } from './content-area/common/services';
import { OrderHistoryComponent, CheckoutComponent } from './content-area/order';
import { CartComponent } from './content-area/cart';
import {  RegisterComponent,
          ProfileComponent,
          ProfileDetailComponent } from './content-area/user';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'product',
    children: [
      { path: '',  component: ProductComponent, canActivate: [AuthGuard]},
      { path: 'details',
          children: [
            { path: '', redirectTo: 'product', pathMatch: 'full'},
            { path: ':id', component: ProductDetailComponent },
        ],
      },
      { path: 'manage', component: ManageProductComponent, canActivate: [AdminGuard],
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: ProductListComponent},
          { path: 'add', component: ProductAddComponent},
          { path: 'edit', component: ProductEditComponent}
        ]
      },
    ]
  },
  { path: 'cart', component: CartComponent, canActivate: [AuthGuard] },
  { path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'orders', component: OrderHistoryComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [AuthGuard, AdminGuard]
})
export class AppRoutingModule { }
