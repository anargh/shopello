import { AuthenticateService } from '../../app/content-area/common';
import { User } from '../../app/content-area/user';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class MockUserService {

  registerUser(user: User): Observable<User> {
    return Observable.of(user);
  }
}
