import { Subject } from 'rxjs/Subject';
import { User } from '../../app/content-area/user';
import { Observable } from 'rxjs/Observable';

export class MockAuthenticateService {

    private usersSubject: Subject<User> = new Subject();

    checkLogin(email: string): Observable<User> {
        const user: User = {
            email: 'test@gmail.com',
            password: '123456'
        };
        this.usersSubject.next(user);
        return this.usersSubject.asObservable();
    }
}
